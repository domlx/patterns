<?php


namespace Behavioral\State;


class PauseState implements State
{
    public function proceedToNext(PlayerContext $context)
    {
        $context->setState(new PlayState());
    }

    public function toString(): string
    {
        return 'Pause state.';
    }
}