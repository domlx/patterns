<?php


namespace Behavioral\State;


class PlayState implements State
{

    public function proceedToNext(PlayerContext $context)
    {
        $context->setState(new StopState());
    }

    public function toString(): string
    {
        return 'Play state.';
    }
}