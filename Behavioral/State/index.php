<?php

use Behavioral\State\PauseState;
use Behavioral\State\PlayerContext;

require __DIR__ . '/../../vendor/autoload.php';

$player = PlayerContext::create();

echo $player->toString() . PHP_EOL;

$player->setState(new PauseState());
echo $player->toString() . PHP_EOL;

$player->proceedToNext();
echo $player->toString() . PHP_EOL;

$player->proceedToNext();
echo $player->toString() . PHP_EOL;

$player->proceedToNext();
echo $player->toString() . PHP_EOL;
