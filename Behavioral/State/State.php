<?php

namespace Behavioral\State;

interface State
{
    public function proceedToNext(PlayerContext $context);

    public function toString(): string;
}