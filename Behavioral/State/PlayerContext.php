<?php

namespace Behavioral\State;

class PlayerContext
{
    private $state;

    public static function create(): PlayerContext
    {
        $player = new self();
        $player->state = new PlayState();

        return $player;
    }

    public function setState(State $state)
    {
        $this->state = $state;
    }

    public function proceedToNext()
    {
        $this->state->proceedToNext($this);
    }

    public function toString()
    {
        return $this->state->toString();
    }
}