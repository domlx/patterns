<?php


namespace Behavioral\State;


class StopState implements State
{
    public function proceedToNext(PlayerContext $context)
    {
        $context->setState(new PlayState());
    }

    public function toString(): string
    {
        return 'Stop state.';
    }
}