<?php

use Behavioral\Iterator\CsvIterator;

require __DIR__ . '/../../vendor/autoload.php';

$time_start = microtime(true);
echo 'Start memory: ' . memory_get_usage() . PHP_EOL;

$csv = new CsvIterator(__DIR__ . '/users.csv');

foreach ($csv as $key => $row) {
    if($key === 100 || $key === 2500 || $key === 4999) {
        print_r($row);
    }
}
$time_end = microtime(true);

echo 'Iterator memory used: ' . memory_get_usage() . PHP_EOL;
echo 'Iterator memory peak: ' . memory_get_peak_usage(true) . PHP_EOL;

echo $time_end - $time_start . PHP_EOL;


$time_start = microtime(true);
echo 'Start memory: ' . memory_get_usage() . PHP_EOL;
$row = 1;
$handle = fopen(__DIR__ . '/users.csv', "r");
$data = fgetcsv($handle, 1000, ",");
$num = count($data);
$row++;
foreach ($data as $key => $row) {
    if ($key === 100 || $key === 2500 || $key === 4999) {
        print_r($row);
    }
}
fclose($handle);
$time_end = microtime(true);

echo 'Parser memory used: ' . memory_get_usage() . PHP_EOL;
echo 'Parser memory peak: ' . memory_get_peak_usage(true) . PHP_EOL;


echo $time_end - $time_start . PHP_EOL;