<?php

use Behavioral\Mediator\Event;
use Behavioral\Mediator\Logger;
use Behavioral\Mediator\OnboardingNotification;
use Behavioral\Mediator\UserRepository;
use Behavioral\Mediator\Events;

require __DIR__ . '/../../vendor/autoload.php';

$event = new Event();
$repository = new UserRepository($event);

$logger = new Logger(__DIR__ . "/log.txt");
Events::getInstance()->attach($logger, $event->setEvent("*"));

$onboarding = new OnboardingNotification("1@example.com");
Events::getInstance()->attach($onboarding, $event->setEvent("users:created"));

$repository->initialize(__DIR__ . "users.csv");

$user = $repository->createUser([
                                    "name" => "John Smith",
                                    "email" => "john@smith.com",
                                ]);

$user = $repository->updateUser($user, [
                                    "name" => "John Dou",
                                    "email" => "john@dou.com",
                                ]);

$repository->deleteUser($user);