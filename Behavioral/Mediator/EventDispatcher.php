<?php

namespace Behavioral\Mediator;

class EventDispatcher implements \SplSubject
{
    /**
     * @var array
     */
    private $observers = [];

    public function __construct()
    {
        // The special event group for observers that want to listen to all
        // events.
        $this->observers["*"] = [];
    }

    private function initEventGroup(string $event = "*"): void
    {
        if (!isset($this->observers[$event])) {
            $this->observers[$event] = [];
        }
    }

    private function getEventObservers(string $event = "*"): array
    {
        $this->initEventGroup($event);
        $group = $this->observers[$event];
        $all = $this->observers["*"];

        return array_merge($group, $all);
    }

    public function attach(\SplObserver $observer, Event $event = null): void
    {
        $this->initEventGroup($event->getEvent());

        $this->observers[$event->getEvent()][] = $observer;
    }

    public function detach(\SplObserver $observer, Event $event = null): void
    {
        foreach ($this->getEventObservers($event->getEvent()) as $key => $s) {
            if ($s === $observer) {
                unset($this->observers[$event->getEvent()][$key]);
            }
        }
    }

    public function notify(Event $event = null, object $emitter = null, $data = null): void
    {
        echo "EventDispatcher: Broadcasting the '" . $event->getEvent() . "' event.\n";
        foreach ($this->getEventObservers($event->getEvent()) as $observer) {
            $observer->update($event, $emitter, $data);
        }
    }
}