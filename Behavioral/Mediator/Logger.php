<?php

namespace Behavioral\Mediator;

class Logger implements \SplObserver
{
    private $filename;

    public function __construct($filename)
    {
        $this->filename = $filename;
        if (file_exists($this->filename)) {
            unlink($this->filename);
        }
    }

    public function update(\SplSubject $event, object $emitter = null, $data = null)
    {
        $entry = date("Y-m-d H:i:s") . ": '" . $event->getEvent() . "' with data '" . json_encode($data) . "'\n";
        file_put_contents($this->filename, $entry, FILE_APPEND);

        echo "Logger: Event - '" . $event->getEvent() . "'.\n";
    }
}