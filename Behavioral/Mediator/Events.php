<?php

namespace Behavioral\Mediator;


class Events {

    private static $eventDispatcher;

    private function __construct()
    {
    }

    public static function getInstance(): EventDispatcher
    {
        if(!self::$eventDispatcher) {
            self::$eventDispatcher = new EventDispatcher();
        }
        return self::$eventDispatcher;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}