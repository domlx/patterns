<?php

namespace Behavioral\Mediator;

class UserRepository implements \SplObserver
{
    private $event;

    private $users = [];

    private $events;

    public function __construct(Event $event)
    {
        $this->event = $event;
        $this->events = Events::getInstance();
        $this->events->attach($this, $this->event->setEvent("users:deleted"));
        $this->events->attach($this, $this->event->setEvent("users:updated"));
    }

    public function update(\SplSubject $event, object $emitter = null, $data = null): void
    {
        switch ($event->getEvent()) {
            case "users:deleted":
                if ($emitter === $this) {
                    return;
                }
                $this->deleteUser($data, true);
                break;
            case "users:updated":
                echo "UserRepository: Action on update:user \n";
                break;
        }
    }


    public function initialize(string $filename): void
    {
        echo "UserRepository: Loading user records from a file.\n";
        $this->events->notify($this->event->setEvent("users:init"), $this, $filename);
    }

    public function createUser(array $data, bool $silent = false): User
    {
        echo "UserRepository: Creating a user.\n";

        $user = new User();
        $user->update($data);

        $id = bin2hex(openssl_random_pseudo_bytes(16));
        $user->update(["id" => $id]);
        $this->users[$id] = $user;

        if (!$silent) {
            $this->events->notify($this->event->setEvent("users:created"), $this, $user);
        }

        return $user;
    }

    public function updateUser(User $user, array $data, bool $silent = false): ?User
    {
        echo "UserRepository: Updating a user.\n";

        $id = $user->attributes["id"];
        if (!isset($this->users[$id])) {
            return null;
        }

        $user = $this->users[$id];
        $user->update($data);

        if (!$silent) {
            $this->events->notify($this->event->setEvent("users:updated"), $this, $user);
        }

        return $user;
    }

    public function deleteUser(User $user, bool $silent = false): void
    {
        echo "UserRepository: Deleting a user.\n";

        $id = $user->attributes["id"];
        if (!isset($this->users[$id])) {
            return;
        }

        unset($this->users[$id]);

        if (!$silent) {
            $this->events->notify($this->event->setEvent("users:deleted"), $this, $user);
        }
    }
}