<?php

namespace Behavioral\Mediator;

class OnboardingNotification implements \SplObserver
{
    private $adminEmail;

    public function __construct(string $adminEmail)
    {
        $this->adminEmail = $adminEmail;
    }

    public function update(\SplSubject $event, object $emitter = null, $data = null): void
    {
        echo "OnboardingNotification: The notification has been emailed to email: $this->adminEmail\n";
    }
}