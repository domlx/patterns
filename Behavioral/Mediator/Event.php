<?php


namespace Behavioral\Mediator;


use SplObserver;

class Event implements \SplSubject
{
    protected $event = '*';

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @param string $event
     * @return Event
     */
    public function setEvent(string $event): Event
    {
        $this->event = $event;
        return $this;
    }

    public function attach(SplObserver $observer)
    {
        // TODO: Implement attach() method.
    }

    public function detach(SplObserver $observer)
    {
        // TODO: Implement detach() method.
    }

    public function notify()
    {
        // TODO: Implement notify() method.
    }
}