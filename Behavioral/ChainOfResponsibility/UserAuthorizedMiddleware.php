<?php

namespace Behavioral\ChainOfResponsibility;

class UserAuthorizedMiddleware extends Middleware
{
    private $server;

    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    public function check(string $email, string $password): bool
    {
        if($this->server->hasEmail($email) && $this->server->isValidPassword($email, $password)){
            $this->server->setUser([$email => $password]);
        }

        if (!$this->server->getUser($email)) {
            echo "UserAuthorizedMiddleware: This user ($email) is not authorized!\n";

            return false;
        }

        return parent::check($email, $password);
    }
}