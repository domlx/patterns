<?php

namespace Behavioral\ChainOfResponsibility;

class Middleware
{
    /**
     * @var Middleware
     */
    private $next;

    /**
     * @param Middleware $next
     * @return Middleware
     */
    public function linkWith(Middleware $next): Middleware
    {
        $this->next = $next;

        return $next;
    }

    /**
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function check(string $email, string $password): bool
    {
        if (!$this->next) {
            echo 'Stop, last item of chain' . PHP_EOL;
            return true;
        }

        return $this->next->check($email, $password);
    }

}