<?php


use Behavioral\ChainOfResponsibility\RoleCheckMiddleware;
use Behavioral\ChainOfResponsibility\Server;
use Behavioral\ChainOfResponsibility\UserAuthorizedMiddleware;
use Behavioral\ChainOfResponsibility\UserExistsMiddleware;

require __DIR__ . '/../../vendor/autoload.php';

$server = new Server();
$server->register("admin@example.com", "admin");
$server->register("user@example.com", "user");

$middleware = new UserExistsMiddleware($server);
$middleware
    ->linkWith(new UserAuthorizedMiddleware($server))
    ->linkWith(new RoleCheckMiddleware($server));

$server->setMiddleware($middleware);

echo 'Set wrong credentials' . PHP_EOL;
$server->logIn('sdsd', 'xcx') . PHP_EOL;
echo 'Set wrong password' . PHP_EOL;
$server->logIn('admin@example.com', 'xcx') . PHP_EOL;
echo 'Login user' . PHP_EOL;
$server->logIn('user@example.com', 'user') . PHP_EOL;
echo 'Login admin' . PHP_EOL;
$server->logIn('admin@example.com', 'admin') . PHP_EOL;