<?php

namespace Behavioral\ChainOfResponsibility;

class Server
{
    private $user = [];

    private $users = [];

    /**
     * @var Middleware
     */
    private $middleware;

    /**
     * @param Middleware $middleware
     */
    public function setMiddleware(Middleware $middleware): void
    {
        $this->middleware = $middleware;
    }

    /**
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function logIn(string $email, string $password): bool
    {
        if ($this->middleware->check($email, $password)) {
            echo "Server: Authorization has been successful!\n";

            return true;
        }

        return false;
    }

    /**
     * @param string $email
     * @param string $password
     */
    public function register(string $email, string $password): void
    {
        $this->users[$email] = $password;
    }

    /**
     * @param string $email
     * @return bool
     */
    public function hasEmail(string $email): bool
    {
        return isset($this->users[$email]);
    }

    /**
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function isValidPassword(string $email, string $password): bool
    {
        return $this->users[$email] === $password;
    }

    /**
     * @param $email
     * @return array
     */
    public function getUser($email): array
    {
        return isset($this->user[$email]) ? $this->user : [];
    }

    /**
     * @param array $user
     */
    public function setUser(array $user): void
    {
        $this->user = $user;
    }
}