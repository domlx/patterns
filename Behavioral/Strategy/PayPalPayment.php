<?php

namespace Behavioral\Strategy;

class PayPalPayment implements PaymentMethodInterface
{

    public function getPaymentForm(Order $order): string
    {
        echo 'PayPal payment form' . PHP_EOL;
        return 'PayPal payment form';
    }

    public function validateReturn(Order $order, array $data): bool
    {
        //some validating logic

        echo "PayPalPayment: ...validating... Paid for product: " . $order->getProduct() . PHP_EOL;

        return true;
    }
}
