<?php

namespace Behavioral\Strategy;

class Order
{
    private $id;

    private $product;

    private $price;

    private $email;

    public function setOrder(array $attributes): Order
    {
        foreach ($attributes as $key => $value) {
            $this->{$key} = $value;
        }

        return $this;
    }

    public function get(int $id): self
    {
        // get by id from DB

        return $this->setOrder(['id' => $id]);
    }

    public function save(): Order
    {
        // save to DB and return ID

        return $this;
    }

    public function update(): Order
    {
        // update order

        return $this;
    }

    public function delete(int $id): bool
    {
        // delete order
        return true;
    }

    /**
     * @return string|null
     */
    public function getProduct(): ?string
    {
        return $this->product;
    }
}

