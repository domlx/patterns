<?php

namespace Behavioral\Strategy;

class PrivatBankPayment implements PaymentMethodInterface
{

    public function getPaymentForm(Order $order): string
    {
        echo 'Privat Bank payment QR code' . PHP_EOL;
        return 'Privat Bank payment QR code';
    }

    public function validateReturn(Order $order, array $data): bool
    {
        //some validating logic

        echo "Privat Bank: ...validating... Paid for product: " . $order->getProduct() . PHP_EOL;

        return true;
    }
}
