<?php


use Behavioral\Strategy\Order;
use Behavioral\Strategy\OrderController;
use Behavioral\Strategy\PayPalPayment;
use Behavioral\Strategy\PrivatBankPayment;

require __DIR__ . '/../../vendor/autoload.php';

$order = new Order();
$controller = new OrderController($order);
$paypal = new PayPalPayment();
$private = new PrivatBankPayment();

$order = $controller->saveOrder([
    "email" => "me@example.com",
    "product" => "Beer",
    "price" => 9.95,
]);

$controller->setMethod($paypal);
$controller->getPaymentForm($order);
$controller->getPaymentReturn($order, []);

$controller->saveOrder([
    "email" => "me@example.com",
    "product" => "Burger",
    "total" => 19.95,
]);

$controller->setMethod($private);
$controller->getPaymentForm($order);
$controller->getPaymentReturn($order, []);

