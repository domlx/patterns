<?php

namespace Behavioral\Strategy;

/**
 *
 */
interface PaymentMethodInterface
{
    /**
     * @param Order $order
     * @return string
     */
    public function getPaymentForm(Order $order): string;

    /**
     * @param Order $order
     * @param array $data
     * @return bool
     */
    public function validateReturn(Order $order, array $data): bool;
}
