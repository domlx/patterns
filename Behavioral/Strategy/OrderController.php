<?php

namespace Behavioral\Strategy;

/**
 *
 */
class OrderController
{
    /** @var Order */
    protected $order;

    /** @var PaymentMethodInterface */
    protected $method;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param mixed $method
     */
    public function setMethod(PaymentMethodInterface $method): void
    {
        $this->method = $method;
    }

    /**
     * @param int $id
     * @return Order
     */
    public function getOrder(int $id): Order
    {
        echo "Controller: GET request." . PHP_EOL;
        return $this->order->get($id);
    }

    /**
     * @param array $data
     * @return Order
     */
    public function saveOrder(array $data): Order
    {
        echo "Controller: Created the order." . PHP_EOL;
        return $this->order->setOrder($data)->save();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteOrder(int $id): bool
    {
        echo "Controller: POST request." . PHP_EOL;
        return $this->order->delete($id);
    }

    /**
     *
     */
    public function getAllOrders(): void
    {
        echo "Controller: Get all orders:\n";
    }

    /**
     * @param Order $order
     * @return mixed
     */
    public function getPaymentForm(Order $order)
    {
        return $this->method->getPaymentForm($order);
    }

    /**
     * @param Order $order
     * @param array $data
     */
    public function getPaymentReturn(Order $order, array $data): void
    {
        try {
            if ($this->method->validateReturn($order, $data)) {
                echo "Controller: Thanks for your order!\n";
                $order->update();
            }
        } catch (\Exception $e) {
            echo "Controller: got an exception (" . $e->getMessage() . ")\n";
        }
    }
}