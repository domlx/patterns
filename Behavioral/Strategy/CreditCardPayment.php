<?php

namespace Behavioral\Strategy;

class CreditCardPayment implements PaymentMethodInterface
{

    public function getPaymentForm(Order $order): string
    {
        echo 'Credit card payment form' . PHP_EOL;
        return 'Credit card payment form';
    }

    public function validateReturn(Order $order, array $data): bool
    {
        //some validating logic

        echo "CreditCardPayment: ...validating... Paid for product: " . $order->getProduct() . PHP_EOL;

        return true;
    }
}
