<?php

namespace Behavioral\Command;

interface Command
{
    public function execute(int $value, int $actionValue): float;

    public function undo(int $actionValue): float;

}