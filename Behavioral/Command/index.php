<?php


use Behavioral\Command\AddCommand;
use Behavioral\Command\Calculator;
use Behavioral\Command\DivideCommand;
use Behavioral\Command\MultiplyCommand;
use Behavioral\Command\SubtractCommand;

require __DIR__ . '/../../vendor/autoload.php';

$add = new AddCommand();
$sub = new SubtractCommand();
$div = new DivideCommand();
$mul = new MultiplyCommand();
$calc = new Calculator($add, $sub, $mul, $div);

$calc->run();