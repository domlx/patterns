<?php

namespace Behavioral\Command;

/**
 * Class Calculator
 * @package Behavioral\Command
 */
class Calculator
{
    /**
     * @var
     */
    protected $value;

    /**
     * @var
     */
    protected $actionValue;

    /**
     * @var
     */
    protected $command;

    /**
     * @var AddCommand
     */
    protected $add;

    /**
     * @var SubtractCommand
     */
    protected $sub;

    /**
     * @var MultiplyCommand
     */
    protected $mul;

    /**
     * @var DivideCommand
     */
    protected $div;

    /**
     * Calculator constructor.
     * @param AddCommand $add
     * @param SubtractCommand $sub
     * @param MultiplyCommand $mul
     * @param DivideCommand $div
     */
    public function __construct(AddCommand $add, SubtractCommand $sub, MultiplyCommand $mul, DivideCommand $div)
    {
        $this->add = $add;
        $this->sub = $sub;
        $this->mul = $mul;
        $this->div = $div;
    }

    /**
     * @param int $actionValue
     */
    public function executeCommand(int $actionValue)
    {
        $this->value = $this->command->execute($this->value, $actionValue);
        echo "\nResult: $this->value\n";
    }

    /**
     * @param int $actionValue
     */
    public function undoCommand(int $actionValue)
    {
        $this->value = $this->command->undo($actionValue);
        echo "\nResult: $this->value\n";
    }

    /**
     * @param mixed $command
     */
    public function setCommand($command): Calculator
    {
        $this->command = $command;
        return $this;
    }

    /**
     * Run commands
     */
    public function run()
    {
        echo "\nEnter value:\n";
        $this->value = readline();
        if($this->value === 'q' || !is_numeric($this->value)){
            echo "Counting quit or value is not a number!\n";
            return;
        }
        do {
            echo "\nEnter action (add, sub, mul, div, undo):\n";
            $action = readline();

            if($action === 'undo' && $this->command){
                $this->undoCommand($this->actionValue);
            }else {
                if (!property_exists($this, $action)) {
                    echo "Action not exists ($action)!\n";
                    break;
                }

                echo "Enter value:\n";
                $this->actionValue = readline();
                $this->setCommand($this->$action);
                $this->executeCommand($this->actionValue);
            }
        } while ($action !== 'q' || $this->actionValue !== 'q');
        echo "Counting stopped!\n";
    }
}