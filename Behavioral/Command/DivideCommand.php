<?php

namespace Behavioral\Command;

class DivideCommand implements Command
{
    protected $result;

    public function execute(int $value, int $actionValue): float
    {
        $this->result = $value / $actionValue;
        return $this->result;
    }

    public function undo(int $actionValue): float
    {
        $this->result = $this->result * $actionValue;
        return $this->result;
    }
}
