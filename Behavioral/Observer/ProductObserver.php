<?php

namespace Behavioral\Observer;

use Behavioral\Mediator\Event;
use Behavioral\Mediator\Events;
use SplObserver;

class ProductObserver implements SplObserver
{
    private $event;

    private $events;

    public function __construct(Event $event)
    {
        $this->event = $event;
        $this->events = Events::getInstance();
        $this->events->attach($this, $this->event->setEvent("product:updated"));
        $this->events->attach($this, $this->event->setEvent("product:notAvailable"));
        $this->events->attach($this, $this->event->setEvent("product:deleted"));
    }

    public function update(\SplSubject $event, object $emitter = null): void
    {
        switch ($event->getEvent()) {
            case "product:deleted":
                if ($emitter === $this) {
                    return;
                }
                $this->deleteProduct($emitter);
                break;
            case "product:updated":
                echo "UserRepository: Action on update:user \n";
                break;
        }
    }

    public function createProduct(array $data): Product
    {
        echo "ProductRepository: Creating a product.\n";

        // create product
        $product = new Product();
        $product->update($data);

        $this->events->notify($this->event->setEvent("product:created"), $this, $product);

        return $product;
    }

    public function updateProduct(Product $product, array $data): ?Product
    {
        echo "ProductRepository: Updating a product.\n";

        // update product
        $product->update($data);

        $this->events->notify($this->event->setEvent("product:updated"), $this, $product);

        return $product;
    }

    public function deleteProduct(Product $product): void
    {
        echo "ProductRepository: Deleting a product.\n";

        // mark product as deleted

        $this->events->notify($this->event->setEvent("product:deleted"), $this, $product);
    }
}