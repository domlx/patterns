<?php

use Behavioral\Mediator\Event;
use Behavioral\Mediator\Logger;
use Behavioral\Mediator\Events;
use Behavioral\Observer\ProductObserver;

require __DIR__ . '/../../vendor/autoload.php';

$event = new Event();
$repository = new ProductObserver($event);

$logger = new Logger(__DIR__ . "/log.txt");
Events::getInstance()->attach($logger, $event->setEvent("*"));

$product = $repository->createProduct([
                                    "name" => "Product1",
                                    "type" => "type1",
                                ]);

$product = $repository->updateProduct($product, [
                                    "name" => "Product2",
                                    "type" => "type2",
                                ]);

$repository->deleteProduct($product);