<?php

namespace Behavioral\Memento;

class EditorStateWrapper
{
    private $currentState;

    public function __construct()
    {
        $this->currentState = new Editor(Editor::PAGE_CREATED);
    }

    public function open()
    {
        $this->currentState = new Editor(Editor::PAGE_OPENED);
    }

    public function assign()
    {
        $this->currentState = new Editor(Editor::PAGE_ASSIGNED);
    }

    public function close()
    {
        $this->currentState = new Editor(Editor::PAGE_CLOSED);
    }

    public function saveToMemento(): Memento
    {
        return new Memento(clone $this->currentState);
    }

    public function restoreFromMemento(Memento $memento)
    {
        $this->currentState = $memento->getState();
    }

    public function getEditor(): Editor
    {
        return $this->currentState;
    }
}