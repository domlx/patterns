<?php

namespace Behavioral\Memento;

use Symfony\Component\OptionsResolver\Exception\InvalidArgumentException;

class Editor implements \Stringable
{
    const PAGE_CREATED = 'created';
    const PAGE_OPENED = 'opened';
    const PAGE_ASSIGNED = 'assigned';
    const PAGE_CLOSED = 'closed';

    private $state;

    /**
     * @var string[]
     */
    private static $validStates = [
        self::PAGE_CREATED,
        self::PAGE_OPENED,
        self::PAGE_ASSIGNED,
        self::PAGE_CLOSED,
    ];

    public function __construct(string $state)
    {
        self::ensureIsValidState($state);

        $this->state = $state;
    }

    private static function ensureIsValidState(string $state)
    {
        if (!in_array($state, self::$validStates)) {
            throw new InvalidArgumentException('Invalid state given');
        }
    }

    public function __toString(): string
    {
        return $this->state;
    }
}
