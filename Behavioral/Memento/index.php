<?php

use Behavioral\Memento\EditorStateWrapper;

require __DIR__ . '/../../vendor/autoload.php';

$memento = new EditorStateWrapper();

$memento->open();
echo $memento->getEditor() . PHP_EOL;
$editorMementoOpen = $memento->saveToMemento();

$memento->assign();
echo $memento->getEditor() . PHP_EOL;
$editorMementoAssign = $memento->saveToMemento();

$memento->close();
echo $memento->getEditor() . PHP_EOL;

$memento->restoreFromMemento($editorMementoAssign);
echo $memento->getEditor() . PHP_EOL;
echo $editorMementoAssign->getStateDate() . PHP_EOL;

$memento->restoreFromMemento($editorMementoOpen);
echo $memento->getEditor() . PHP_EOL;
echo $editorMementoOpen->getStateDate() . PHP_EOL;
