<?php

namespace Behavioral\Memento;

class Memento
{
    private $state;

    private $datetime;

    public function __construct( Editor $state)
    {
        $this->state = $state;
        $this->datetime = date('Y-m-d H:i:s', time());
        sleep(2);
    }

    public function getState(): Editor
    {
        return $this->state;
    }

    public function getStateDate(): string
    {
        return $this->datetime;
    }
}
