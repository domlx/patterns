<?php

namespace Creational\Singleton;

use PDO;

/**
 * Class DB
 * @package Singleton
 */
class DB {

    /**
     * @var PDO
     */
    private $_connection;

    /**
     * Single instance
     */
    private static $_instance;

    /**
     * DB constructor.
     */
    private function __construct() {
        $dsn = sprintf('%s:host=%s;dbname=%s;port=%d',
                       $_ENV['db.type'], $_ENV['db.host'], $_ENV['db.name'], $_ENV['db.port']);
        $pdo = new PDO($dsn, $_ENV['db.user'], $_ENV['db.pass']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->_connection = $pdo;
    }

    /**
     * @return DB
     */
    public static function getInstance(): DB
    {
        if(!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->_connection;
    }
}