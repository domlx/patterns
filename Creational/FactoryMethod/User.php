<?php

namespace Creational\FactoryMethod;

/**
 * Class User
 * @package FactoryMethod
 */
class User
{
    /**
     * @return array
     */
    public function getUserData(): array
    {
        return [];
    }
}