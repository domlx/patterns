<?php

namespace Creational\FactoryMethod;

/**
 * Class BusinessAccount
 * @package FactoryMethod
 */
class BusinessAccount implements AccountInterface
{
    /**
     * @param array $data
     * @return bool
     */
    public function open(array $data): bool
    {
        echo 'Business Account created' . PHP_EOL;
        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function close(int $id): bool
    {
        echo 'Business Account closed' . PHP_EOL;
        return true;
    }
}