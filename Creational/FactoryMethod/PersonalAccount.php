<?php

namespace Creational\FactoryMethod;

/**
 * Class PersonalAccount
 * @package FactoryMethod
 */
class PersonalAccount implements AccountInterface
{
    /**
     * @param array $data
     * @return bool
     */
    public function open(array $data): bool
    {
        echo 'Personal Account created' . PHP_EOL;
        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function close(int $id): bool
    {
        echo 'Personal Account closed' . PHP_EOL;
        return true;
    }
}