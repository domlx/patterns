<?php

namespace Creational\FactoryMethod;

/**
 * Class PersonalAccountCreator
 * @package FactoryMethod
 */
class PersonalAccountCreator extends AccountCreator
{
    /**
     * @return AccountInterface
     */
    public function getAccount(): AccountInterface
    {
        return new PersonalAccount();
    }

    /**
     * @return array
     */
    public function getPersonalData(): array
    {
        // some logic
        return $this->user->getUserData();
    }
}