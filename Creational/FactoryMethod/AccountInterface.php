<?php

namespace Creational\FactoryMethod;

/**
 * Interface Account
 * @package FactoryMethod
 */
interface AccountInterface
{
    /**
     * @param array $data
     * @return bool
     */
    public function open(array $data): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function close(int $id): bool;
}