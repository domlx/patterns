<?php
require __DIR__.'/../../vendor/autoload.php';

use Creational\FactoryMethod\BusinessAccount;
use Creational\FactoryMethod\PersonalAccount;

$personal = new PersonalAccount();
$personal->open([]);
$personal->close(1);

$business = new BusinessAccount();
$business->open([]);
$business->close(1);