<?php

namespace Creational\FactoryMethod;
/**
 * Class AccountCreator
 * @package FactoryMethod
 */
abstract class AccountCreator
{
    /**
     * @var User
     */
    protected $user;

    /**
     * AccountCreator constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return AccountInterface
     */
    abstract public function getAccount(): AccountInterface;

    /**
     * @param array $data
     * @return bool
     */
    public function openAccount(array $data): bool
    {
        $account = $this->getAccount();
        return $account->open($data);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function closeAccount(int $id): bool
    {
        $account = $this->getAccount();
        return $account->close($id);
    }
}