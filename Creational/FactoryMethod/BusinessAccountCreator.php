<?php

namespace Creational\FactoryMethod;

/**
 * Class BusinessAccountCreator
 * @package FactoryMethod
 */
class BusinessAccountCreator extends AccountCreator
{
    /**
     * @return AccountInterface
     */
    public function getAccount(): AccountInterface
    {
        return new BusinessAccount();
    }

    /**
     * @return array
     */
    public function getBusinessData(): array
    {
        // some logic
        return $this->user->getUserData();
    }
}
