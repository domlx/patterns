<?php


namespace Creational\Prototype;


class FactoryInventoryCreator implements InventoryAbstractFactory
{
    protected $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getIp(): Ip
    {
        return new PublicIp($this->value['ip']['type'], $this->value['ip']['id'], $this->value['ip']['counter']);
    }

    public function getRouter(): Router
    {
        return new VirtualRouter(
            $this->value['router']['type'],
            $this->value['router']['id'],
            $this->value['router']['counter'],
            $this->value['router']['port']
        );
    }

    public function getServer(): Server
    {
        return new DNSServer($this->value['server']['type'], $this->value['server']['id'], $this->value['server']['counter']);
    }
}