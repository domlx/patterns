<?php
require __DIR__.'/../../vendor/autoload.php';

use Creational\Prototype\FactoryInventoryCreator;
use Creational\Prototype\DNSServer;
use Creational\Prototype\PrototypeInventoryCreator;
use Creational\Prototype\PublicIp;
use Creational\Prototype\VirtualRouter;

$value = [
    'ip'     => ['type' => 'APNIC', 'id' => 'APNIC_id', 'counter' => 1],
    'server' => ['type' => 'DNS', 'id' => 'APNIC_id', 'counter' => 1],
    'router' => ['type' => 'Virtual', 'id' => 'Virtual_id', 'port' => 80, 'counter' => 1],
];

$time_start = microtime(true);

$network = [];
$ip = new PublicIp($value['ip']['type'], $value['ip']['id'], $value['ip']['counter']);
$router = new VirtualRouter(
    $value['router']['type'],
    $value['router']['id'],
    $value['router']['counter'],
    $value['router']['port']
);
$server = new DNSServer($value['server']['type'], $value['server']['id'], $value['server']['counter']);
echo 'Start memory: ' . memory_get_usage() . PHP_EOL;

for ($i = 100000; $i > 0; $i--) {
    $factory = new PrototypeInventoryCreator($ip, $router, $server);
    $network[] = $factory->getServer();
    $network[] = $factory->getIp();
    $network[] = $factory->getRouter();
}
echo 'Prototype' . PHP_EOL;
echo 'Prototype memory used: ' . memory_get_usage() . PHP_EOL;
echo 'Prototype memory peak: ' . memory_get_peak_usage(true) . PHP_EOL;
echo count($network) . PHP_EOL;
$time_end = microtime(true);

echo $time_end - $time_start . PHP_EOL;

$network = [];
$time_start = microtime(true);
for ($i = 100000; $i > 0; $i--) {
    $factory = new FactoryInventoryCreator($value);
    $network[] = $factory->getServer();
    $network[] = $factory->getIp();
    $network[] = $factory->getRouter();
}

echo 'Factory create' . PHP_EOL;
echo count($network) . PHP_EOL;
$time_end = microtime(true);

echo $time_end - $time_start . PHP_EOL;

echo 'Factory memory used: ' . memory_get_usage() . PHP_EOL;
echo 'Factory memory peak: ' . memory_get_peak_usage(true) . PHP_EOL;