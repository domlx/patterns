<?php


namespace Creational\Prototype;


class PublicIp extends Ip
{

    public function __clone()
    {
        $this->id .= '_ip_copy';
    }
}