<?php

namespace Creational\Prototype;

interface InventoryAbstractFactory
{

    public function getIp(): Ip;

    public function getRouter(): Router;

    public function getServer(): Server;
}