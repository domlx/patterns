<?php


namespace Creational\Prototype;


class PrototypeInventoryCreator implements InventoryAbstractFactory
{
    private $ip;
    private $router;
    private $server;

    public function __construct(Ip $ip, Router $router, Server $server)
    {
        $this->ip = $ip;
        $this->router = $router;
        $this->server = $server;
    }

    function getIp(): Ip
    {
        return clone $this->ip;
    }

    function getRouter(): Router
    {
        return clone $this->router;
    }

    function getServer(): Server
    {
        return clone $this->server;
    }
}