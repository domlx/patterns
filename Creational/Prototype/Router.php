<?php


namespace Creational\Prototype;


abstract class Router extends Inventory
{
    protected $port;

    public function __construct($type, $id, $port, $counter)
    {
        parent::__construct($type, $id, $counter);
        $this->port = $port;
    }
}