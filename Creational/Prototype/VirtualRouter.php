<?php


namespace Creational\Prototype;


class VirtualRouter extends Router
{

    public function __clone()
    {
        $this->id .= '_router_copy';
    }
}