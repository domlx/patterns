<?php

namespace Creational\Prototype;

abstract class Inventory
{
    protected $type;

    protected $id;

    protected $counter;

    public function __construct($type, $id, $counter)
    {
        $this->type = $type;
        $this->id = $id;
        $this->counter = $counter;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getCounter(): int
    {
        return $this->counter;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function setType($type): void
    {
        $this->type = $type;
    }

    public function setCounter($counter): void
    {
        $this->counter = $counter;
    }

    abstract public function __clone();
}