<?php


namespace Creational\Prototype;


class DNSServer extends Server
{

    public function __clone()
    {
        $this->id .= '_dns_server_copy';
    }
}