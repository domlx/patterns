<?php

require __DIR__ . '/../../vendor/autoload.php';

use Creational\AbstractFactory\AppleLaptop;
use Creational\AbstractFactory\HPLaptop;


$apple = new AppleLaptop(100, 500);
echo $apple->getProcessor()->getPowerUsage();
echo $apple->getStorage()->getFreeSpace() . PHP_EOL;

$hp = new HPLaptop(200, 600);
echo $hp->getProcessor()->getPowerUsage();
echo $hp->getStorage()->getFreeSpace();