<?php

namespace Creational\AbstractFactory;

class ToshibaStorage implements Storage
{
    protected $memory;

    public function __construct($memory)
    {
        $this->memory = $memory;
    }

    public function getFreeSpace(): string
    {
        return "Toshiba storage: $this->memory" . PHP_EOL;
    }
}
