<?php

namespace Creational\AbstractFactory;

interface Processor
{
    public function getPowerUsage(): string;
}
