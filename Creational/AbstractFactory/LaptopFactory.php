<?php

namespace Creational\AbstractFactory;

interface LaptopFactory
{
    public function getProcessor(): Processor;

    public function getStorage(): Storage;
}
