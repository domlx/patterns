<?php

namespace Creational\AbstractFactory;

class IntelProcessor implements Processor
{
    protected $power;

    public function __construct($power)
    {
        $this->power = $power;
    }

    public function getPowerUsage(): string
    {
        return "Intel processor: $this->power" . PHP_EOL;
    }
}
