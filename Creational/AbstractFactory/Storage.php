<?php

namespace Creational\AbstractFactory;

interface Storage
{
    public function getFreeSpace(): string;
}
