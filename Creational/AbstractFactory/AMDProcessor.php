<?php

namespace Creational\AbstractFactory;

class AMDProcessor implements Processor
{
    protected $power;

    public function __construct($power)
    {
        $this->power = $power;
    }

    public function getPowerUsage(): string
    {
        return "AMD processor: $this->power" . PHP_EOL;
    }
}
