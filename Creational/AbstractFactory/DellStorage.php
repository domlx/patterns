<?php

namespace Creational\AbstractFactory;

class DellStorage implements Storage
{
    protected $memory;

    public function __construct($memory)
    {
        $this->memory = $memory;
    }

    public function getFreeSpace(): string
    {
        return "Dell storage: $this->memory" . PHP_EOL;
    }
}
