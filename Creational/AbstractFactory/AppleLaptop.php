<?php

namespace Creational\AbstractFactory;

class AppleLaptop implements LaptopFactory
{
    protected const TYPE = 'Apple Laptop';

    protected $power;

    protected $memory;

    public function __construct($power, $memory)
    {
        $this->power = $power;
        $this->memory = $memory;
        echo self::TYPE . PHP_EOL;
    }

    public function getProcessor(): Processor
    {
        return new IntelProcessor($this->power);
    }

    public function getStorage(): Storage
    {
        return new ToshibaStorage($this->memory);
    }
}
