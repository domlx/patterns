<?php

namespace Creational\AbstractFactory;

class HPLaptop implements LaptopFactory
{
    protected const TYPE = 'HP Laptop';

    protected $power;

    protected $memory;

    public function __construct($power, $memory)
    {
        $this->power = $power;
        $this->memory = $memory;
        echo self::TYPE . PHP_EOL;
    }

    public function getProcessor(): Processor
    {
        return new AMDProcessor($this->power);
    }

    public function getStorage(): Storage
    {
        return new DellStorage($this->memory);
    }
}
