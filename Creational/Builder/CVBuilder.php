<?php

namespace Creational\Builder;

interface CVBuilder
{
    public function setFullName(string $name): CVBuilder;

    public function setDateOfBirth(string $date): CVBuilder;

    public function setExperience(array $list): CVBuilder;

    public function setSkills(array $list): CVBuilder;

    public function generate(): string;
}
