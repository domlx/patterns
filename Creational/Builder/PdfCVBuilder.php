<?php

namespace Creational\Builder;

class PdfCVBuilder implements CVBuilder
{
    protected $base;

    public function __construct()
    {
        $this->base = 'CV' . PHP_EOL;
    }

    public function setFullName(string $name): CVBuilder
    {
        $this->base .= ucwords($name) . PHP_EOL;

        return $this;
    }

    public function setDateOfBirth(string $date): CVBuilder
    {
        $this->base .= date('Y-m-d', strtotime($date)) . PHP_EOL;

        return $this;
    }

    public function setSkills(array $list): CVBuilder
    {
        $skills = $list ? 'Skills: ' . PHP_EOL: '';
        foreach ($list as $item){
            $skills .= trim($item) . ';' . PHP_EOL;
        }
        $this->base .= $skills . PHP_EOL;

        return $this;
    }

    public function setExperience(array $list): CVBuilder
    {
        $experience = $list ? 'Experience: ' . PHP_EOL: '';
        foreach ($list as $item){
            $experience .= trim($item) . ';' . PHP_EOL;
        }
        $this->base .= $experience . PHP_EOL;

        return $this;
    }

    public function generate(): string
    {
        $this->base = 'PDV file CV' . PHP_EOL . $this->base;

        return $this->base;
    }
}