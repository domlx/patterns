<?php

use Creational\Builder\PdfCVBuilder;

require __DIR__ . '/../../vendor/autoload.php';

$cvPdf = new PdfCVBuilder();
echo $cvPdf->setFullName('first user')
    ->setDateOfBirth('September 1, 1990')
    ->setSkills(['one', 'two', 'three'])
    ->setExperience(['first company', 'second company'])
    ->generate();
