<?php

require __DIR__ . '/../../vendor/autoload.php';

use Structural\Composite\Button;
use Structural\Composite\Div;
use Structural\Composite\Link;
use Structural\Composite\Paragraph;

$div = new Div();
$link = new Link('localhost', 'https://localhost.test');
$button = new Button('Button test');
$paragraph = new Paragraph('Testing link: ');
$paragraph->add($link);

echo $div->add($paragraph)->add($link)->add($button)->render();
