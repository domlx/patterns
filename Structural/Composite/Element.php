<?php


namespace Structural\Composite;


abstract class Element implements Component
{
    protected $_children = [];

    public function add(Component $Component): Component
    {
        $this->_children[] = $Component;
        return $this;
    }

    /**
     * @throws \Structural\Composite\ComponentException
     */
    public function getChild($index): Component
    {
        if (!isset($this->_children[$index])) {
            throw new ComponentException("Child not exists");
        }

        return $this->_children[$index];
    }

    /**
     * @throws \Structural\Composite\ComponentException
     */
    public function remove($index): Component
    {
        if (!isset($this->_children[$index])) {
            throw new ComponentException("Child not exists");
        }
        unset($this->_children[$index]);

        return $this;
    }

    public function getChildren(): array
    {
        return $this->_children;
    }
}