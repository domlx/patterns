<?php


namespace Structural\Composite;


class Link extends Element
{
    private $text;

    private $url;

    public function __construct(string $text, string $url)
    {
        $this->text = $text;
        $this->url = $url;
    }

    public function render(): string
    {
        return "<a href=". $this->url ." >" . $this->text . "</a>";
    }
}