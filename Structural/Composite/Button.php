<?php


namespace Structural\Composite;


class Button extends Element
{
    private $text;

    public function __construct(string $text)
    {
        $this->text = $text;
    }

    public function render(): string
    {
        return "<button type='button' >" . $this->text . "</button>";
    }
}