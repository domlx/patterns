<?php


namespace Structural\Composite;


class Paragraph extends Element
{
    private $text;

    public function __construct(string $text)
    {
        $this->text = $text;
    }

    public function render(): string
    {
        $formCode = '<p>' . $this->text;

        foreach ($this->_children as $element) {
            $formCode .= $element->render();
        }

        return $formCode . '</p>';
    }
}