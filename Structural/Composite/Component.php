<?php


namespace Structural\Composite;


interface Component
{
    public function add(Component $Component): Component;

    public function remove($index): Component;

    public function getChild($index);

    public function getChildren(): array;

    public function render(): string;
}