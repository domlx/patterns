<?php


namespace Structural\Composite;


class Div extends Element
{

    public function render(): string
    {
        $formCode = '<div>';

        foreach ($this->_children as $element) {
            $formCode .= $element->render();
        }

        return $formCode . '</div>';
    }
}