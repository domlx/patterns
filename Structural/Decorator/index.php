<?php

use Structural\Decorator\ExtraBed;
use Structural\Decorator\ParkingPlace;
use Structural\Decorator\RoomBooking;

require __DIR__ . '/../../vendor/autoload.php';

$roomBooking = new RoomBooking(50, 'Double room with see view');
echo 'Price: ' . $roomBooking->calculatePrice() . '. Description: '. $roomBooking->getDescription() . PHP_EOL;

$roomBooking = new ExtraBed($roomBooking);
echo 'Price: ' . $roomBooking->calculatePrice() . '. Description: '. $roomBooking->getDescription() . PHP_EOL;

$roomBooking = new ParkingPlace($roomBooking);
echo 'Price: ' . $roomBooking->calculatePrice() . '. Description: '. $roomBooking->getDescription() . PHP_EOL;