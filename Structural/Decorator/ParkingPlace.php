<?php


namespace Structural\Decorator;


class ParkingPlace extends BookingDecorator
{
    private const PRICE = 5;

    public function calculatePrice(): int
    {
        return $this->booking->calculatePrice() + self::PRICE;
    }

    public function getDescription(): string
    {
        return $this->booking->getDescription() . ' + parking place';
    }
}