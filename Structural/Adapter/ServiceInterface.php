<?php

namespace Structural\Adapter;

interface ServiceInterface
{
    public function authenticate(array $options);

    public function post(array $data);
}