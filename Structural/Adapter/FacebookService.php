<?php


namespace Structural\Adapter;

use Facebook\Facebook;

class FacebookService implements ServiceInterface
{
    protected $fb;

    public function __construct()
    {
        $this->authenticate(['app-id' => 'test', 'app-secret' => 'test']);
    }

    /**
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function authenticate(array $options)
    {
        $this->fb = new Facebook(
            [
                'app_id'                => "{$options['app-id']}",
                'app_secret'            => "{$options['app-secret']}",
                'default_graph_version' => 'v2.3',
            ]
        );
    }

    public function post(array $data)
    {
        return $this->fb->post('/me/feed',
                               [
                                   'message' => $data['description'],
                                   'title'   => $data['title'],
                               ]
        );
    }
}