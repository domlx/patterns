<?php


namespace Structural\Adapter;


use TwitterAPIExchange;

class TwitterService implements ServiceInterface
{

    protected $twitter;

    public function __construct()
    {
        $this->authenticate(
            [
                'oauth_access_token'        => "YOUR_OAUTH_ACCESS_TOKEN",
                'oauth_access_token_secret' => "YOUR_OAUTH_ACCESS_TOKEN_SECRET",
                'consumer_key'              => "YOUR_CONSUMER_KEY",
                'consumer_secret'           => "YOUR_CONSUMER_SECRET",
            ]
        );
    }

    public function authenticate(array $options)
    {
        $this->twitter = new TwitterAPIExchange($options);
    }

    public function post(array $data)
    {
        $this->twitter->buildOauth('url', 'POST')
            ->setPostfields($data)
            ->performRequest();
    }
}