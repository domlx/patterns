<?php

require __DIR__ . '/../../vendor/autoload.php';

use Structural\Adapter\FacebookService;
use Structural\Adapter\Post;
use Structural\Adapter\TwitterService;

$post = new Post();
$id = $post->setTitle('Test title')
    ->setDescription('test description')
    ->save();

$data = [
    'title'       => $post->getTitle(),
    'description' => $post->getDescription().'localhost.test/post/'
        .$id,
];

// adapters
$facebook = new FacebookService();
$facebook->post($data);

$twitter = new TwitterService();
$twitter->post($data);
