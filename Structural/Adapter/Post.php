<?php

namespace Structural\Adapter;

class Post
{
    protected $description;

    protected $title;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $title
     * @return \Structural\Adapter\Post
     */
    public function setTitle(string $title): Post
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $description
     * @return \Structural\Adapter\Post
     */
    public function setDescription(string $description): Post
    {
        $this->description = $description;
        return $this;
    }

    public function post(): ?int
    {
        // Code to save post to DB
        try {
            return $this->save();
        } catch (\Exception $e) {
            return null;
        }
    }

    public function save(): int
    {
        // return post ID
        return 1;
    }
}