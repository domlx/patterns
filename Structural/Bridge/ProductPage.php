<?php


namespace Structural\Bridge;


class ProductPage extends Page
{
    protected $product;

    public function __construct(Renderer $renderer, Product $product)
    {
        parent::__construct($renderer);
        $this->product = $product;
    }

    public function view(): string
    {
        return $this->renderer->renderParts(
            [
                $this->renderer->renderHeader(),
                $this->renderer->renderTitle($this->product->getTitle()),
                $this->renderer->renderTextBlock(
                    $this->product->getDescription()
                ),
                $this->renderer->renderImage($this->product->getImage()),
                $this->renderer->renderLink(
                    "/product/".$this->product->getId(),
                    $this->product->getTitle()
                ),
                $this->renderer->renderFooter(),
                $this->renderer->renderPriceBlock($this->product->getPrice())
            ]
        );
    }
}