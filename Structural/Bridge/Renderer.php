<?php

namespace Structural\Bridge;

interface Renderer
{
    public function renderTitle(string $title): string;

    public function renderTextBlock(string $text): string;

    public function renderImage(string $url): string;

    public function renderLink(string $url, string $title): string;

    public function renderHeader(): string;

    public function renderFooter(): string;

    public function renderComments(): string;

    public function renderPriceBlock(string $price): string;

    public function renderParts(array $parts): string;
}