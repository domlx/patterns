<?php

namespace Structural\Bridge;

interface PageInterface
{

    public function changeRenderer(Renderer $renderer): void;

    public function view(): string;
}
