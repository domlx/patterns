<?php

require __DIR__ . '/../../vendor/autoload.php';

use Structural\Bridge\BlogPage;
use Structural\Bridge\HTMLRenderer;
use Structural\Bridge\JsonRenderer;
use Structural\Bridge\Product;
use Structural\Bridge\ProductPage;

$HTMLRenderer = new HTMLRenderer();
$JSONRenderer = new JsonRenderer();

$dashboard = new BlogPage($HTMLRenderer, "Dashboard", "Welcome message");
$product = new Product(
    "1", "Product One",
    "Lorem ipsum dolor sit amet ...",
    "/images/best_product_ever.jpeg", 39.95
);
$productPage = new ProductPage($HTMLRenderer, $product);

echo 'HTML' . PHP_EOL;
echo $dashboard->view() . PHP_EOL;
echo $productPage->view() . PHP_EOL . PHP_EOL . PHP_EOL;

$dashboard->changeRenderer($JSONRenderer);
$productPage->changeRenderer($JSONRenderer);

echo 'JSON' . PHP_EOL;
echo $dashboard->view() . PHP_EOL;
echo $productPage->view(). PHP_EOL;