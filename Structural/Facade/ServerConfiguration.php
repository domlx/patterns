<?php


namespace Structural\Facade;


interface ServerConfiguration
{
    public function turnOn(): void;

    public function turnOff(): void;

    public function reboot(): void;

    public function reset(): void;

    public function maintenance(array $data): void;
}