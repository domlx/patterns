<?php


namespace Structural\Facade;


class ServerConfigurationFacade implements ServerConfiguration
{
    protected $os;

    protected $ipmi;

    public function __construct(IPMI $ipmi, OS $os)
    {
        $this->ipmi = $ipmi;
        $this->os = $os;
    }

    public function turnOn(): void
    {
        $this->ipmi->switchOn();
        $this->os->launch();
        echo $this->os->getName() . PHP_EOL;
    }

    public function turnOff(): void
    {
        $this->os->stop();
        $this->ipmi->switchOff();
    }

    public function reboot(): void
    {
        if(!$this->os->reboot()){
            $this->reset();
        }
    }

    public function reset(): void
    {
        $this->ipmi->hardReset();
    }

    public function maintenance(array $data): void
    {
        $this->turnOff();
        $this->ipmi->config($data);
        $this->turnOn();
    }
}