<?php


namespace Structural\Facade;


interface OS
{
    public function launch();

    public function stop();

    public function reboot(): bool;

    public function getName(): string;
}