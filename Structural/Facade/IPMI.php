<?php


namespace Structural\Facade;


interface IPMI
{
    public function authorize();

    public function switchOn();

    public function switchOff();

    public function hardReset();

    public function config(array $options);
}