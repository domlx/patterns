<?php


namespace Structural\Facade;


class LinuxOS implements OS
{

    public function launch()
    {
        echo 'Linux Started' . PHP_EOL;
    }

    public function stop()
    {
        echo 'Linux Stopped' . PHP_EOL;
    }

    public function reboot(): bool
    {
        echo 'Linux Reboot' . PHP_EOL;
        return true;
    }

    public function getName(): string
    {
        return 'Linux';
    }
}