<?php


namespace Structural\Facade;


class ILOipmi implements IPMI
{

    public function authorize()
    {
        echo 'User authorized' . PHP_EOL;
    }

    public function switchOn()
    {
        echo 'HP Server switched on' . PHP_EOL;
    }

    public function switchOff()
    {
        echo 'HP Server switched off' . PHP_EOL;
    }

    public function hardReset()
    {
        echo 'HP Server reboot' . PHP_EOL;
    }

    public function config(array $options)
    {
        echo 'HP Server configured: ' . json_encode($options) . PHP_EOL;
    }
}