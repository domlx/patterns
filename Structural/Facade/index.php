<?php

use Structural\Facade\ILOipmi;
use Structural\Facade\LinuxOS;
use Structural\Facade\ServerConfigurationFacade;

require __DIR__ . '/../../vendor/autoload.php';

$os = new LinuxOS();
$ipmi = new ILOipmi();
$server = new ServerConfigurationFacade($ipmi, $os);

$server->maintenance(['test' => 'config']);