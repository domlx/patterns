<?php


namespace Structural\Proxy;


class ServerService
{
    public function getServer(): Server
    {
        return new Server();
    }
}