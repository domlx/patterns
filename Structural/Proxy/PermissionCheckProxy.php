<?php


namespace Structural\Proxy;


class PermissionCheckProxy extends ServerService
{

    /**
     * @throws \Exception
     */
    public function getServer(): Server
    {
        if($this->checkUserPermissions()){
            return parent::getServer();
        }

        throw new \Exception('permission denied');
    }

    protected function checkUserPermissions(): bool
    {
        // permissions check
        return true;
    }
}