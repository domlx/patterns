<?php


namespace Structural\Flyweight;

use Countable;

class ObjectsPool implements Countable
{
    private $objectPool = [];

    public function get(string $name)
    {
        if (!isset($this->charPool[$name])) {
            $this->objectPool[$name] = $this->create($name);
        }

        return $this->objectPool[$name];
    }

    private function create(string $name)
    {
        return call_user_func([$this, 'create' . ucwords($name)], $name);
    }

    public function count(): int
    {
        return count($this->objectPool);
    }

    private function createIpmi(string $name): Ipmi
    {
        return new Ipmi($name);
    }

    private function createRouter(string $name): Router
    {
        return new Router($name);
    }

    private function createSwitches(string $name): Switches
    {
        return new Switches($name);
    }
}