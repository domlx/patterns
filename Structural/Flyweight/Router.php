<?php


namespace Structural\Flyweight;


class Router extends BaseObject
{
    public function getName(): string
    {
        return $this->name;
    }

    // other methods
}