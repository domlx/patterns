<?php


namespace Structural\Flyweight;


class Server
{
    protected $ipmi;
    protected $router;
    protected $switches;

    public function __construct(Ipmi $ipmi, Router $router, Switches $switches)
    {
        $this->ipmi = $ipmi;
        $this->router = $router;
        $this->switches = $switches;
    }

    public function getNames()
    {
        echo 'Server: '.$this->ipmi->getName().' + '.$this->router->getName()
            .' + '.$this->switches->getName().PHP_EOL;
    }

}