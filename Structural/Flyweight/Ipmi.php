<?php


namespace Structural\Flyweight;


class Ipmi extends BaseObject
{
    public function getName(): string
    {
        return $this->name;
    }

    // other methods
}