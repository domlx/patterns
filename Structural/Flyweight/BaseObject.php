<?php

namespace Structural\Flyweight;

abstract class BaseObject
{
    protected $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    abstract public function getName(): string;
}