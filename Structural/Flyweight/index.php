<?php

use Structural\Flyweight\ObjectsPool;
use Structural\Flyweight\Server;

require __DIR__ . '/../../vendor/autoload.php';

$item = new ObjectsPool();

echo $item->count() . PHP_EOL;

$server = new Server($item->get('ipmi'), $item->get('router'), $item->get('switches'));
$server->getNames();
echo $item->count() . PHP_EOL;

$server2 = new Server($item->get('ipmi'), $item->get('router'), $item->get('switches'));
$server2->getNames();
echo $item->count() . PHP_EOL;